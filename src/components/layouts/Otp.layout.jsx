import React, { Component } from 'react';
import Head from "next/head";

class OtpLayout extends Component {
    render() {
    return (
        <>
        <Head>
        <title>OTP Page</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
        />
        </Head>
        {this.props.children}
        </>
    );
    }
}
export default OtpLayout;