import Layout from "../src/components/layouts/Sign.layout";
import Notification from "../src/components/common/Notification";
import Navbar from "../src/components/common/Navbar";
import SignUp from "../pages/sign_up";
import Footer from "../src/components/common/Footer";


export default function Home() {
  return (
    <>
      <Layout>
        <Notification />
        <Navbar />
        <SignUp />
        <Footer />
      </Layout>
    </>
  );
}
