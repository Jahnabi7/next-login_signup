import React, { Component } from "react";
import styles from "../styles/Home.module.css";
import LoginLayout from "../src/components/layouts/Login.layout";
import { Form, Input, Dropdown, FormGroup } from "rsuite";
import "rsuite/dist/styles/rsuite-default.css";


class Login extends Component {
  render() {
    return (
      <>
        <LoginLayout />
        <div className={styles.main}>
          <div className={styles.card}>
            <h3>Login</h3>
            <p className={styles.content}>
              Lorem ipsum dolor sit, amet consectetur
              <br />
              sit amet consectetur adipisicing elit.
            </p>
            <div className="row">
              <div className="col-12 col-sm-8">
                <Form>
                  <FormGroup className={styles.inputContainer}>
                    <i className={styles.phone_icon_login}></i>
                    <Dropdown title="+91" className={styles.Dropdown_login}>
                      <Dropdown.Item>+92</Dropdown.Item>
                      <Dropdown.Item>+93</Dropdown.Item>
                      <Dropdown.Item>+94</Dropdown.Item>
                      <Dropdown.Item>+95</Dropdown.Item>
                    </Dropdown>
                    <Input
                      placeholder="Phone Number"
                      className={styles.input_phone_login}
                    />
                  </FormGroup>
                </Form>

                <div className={styles.Login_note}>
                  Not have an account? <a href="#">Create Account</a>
                </div>
                <div className={styles.btn}>Login</div>
              </div>
              <div className="col-12 col-sm-4">
                <div className={styles.ImageBox}>
                  <p className={styles.para1}></p>
                  <p className={styles.para2}></p>
                  <p className={styles.para3}></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Login;
