import React, { Component } from "react";
import styles from "../styles/Home.module.css";
import OtpLayout from '../src/components/layouts/Otp.layout';
import { Form, Input } from 'rsuite';


class Otp extends Component {
  render() {
    return (
      <>
      <OtpLayout/>
        <div className={styles.main}>
        <div className={styles.card}>
          <h3>OTP Verification</h3>
          <p className={styles.content}>Lorem ipsum dolor sit, amet
            consectetur dolor sit<br/>
            sit amet consectetur adipisicing elit.</p>
          <div className="row">
            <div className="col-12 col-sm-7">
             
            <Form>
                  <Input
                    placeholder="+91 94963 54455"
                    id="Edit"
                    className={styles.Otp_input}
                  /> <a href="#" id="Edit"><i className="fa fa-pencil"></i>Edit</a>
                  </Form>
               <div className={styles.Otp_container}>
             <span className={styles.Otp}>7</span>
             <span className={styles.Otp}>7</span>
             <span className={styles.Otp}>7</span>
             <span className={styles.Otp}>7</span>
             </div>
             <p className={styles.Otp_note}>Didn't receive OTP? <a href="#">Resend it</a></p>
             <div className={styles.btn}>Verify</div>
            </div>
            <div className="col-12 col-sm-5">
            <div className={styles.ImageBox}>
                  <p className={styles.para1}></p>
                  <p className={styles.para2}></p>
                <p className={styles.para3}></p>
              </div>
            </div>
          </div>
        </div>
        </div>
      </>
    );
  }
}

export default Otp;
