import React, { Component } from "react";
import styles from "../styles/Home.module.css";
import { Form, FormGroup, Checkbox, Input, Dropdown } from "rsuite";
import "rsuite/dist/styles/rsuite-default.css";

class SignUp extends Component {
  render() {
    return (
      <>
        <div className={styles.main}>
          <div className={styles.card}>
            <h3>Signup</h3>
            <p className={styles.content}>
              Lorem ipsum dolor sit, amet dolor sit
               consectetur adi<br/>ipsum dolor sit.
            </p>
            <div className="row pt-3">
              <div className="col-12 col-sm-8">
                <Form>
                  <FormGroup className={styles.inputContainer}>
                    <i className="fa fa-user"></i>
                    <Input placeholder="Your name" className={styles.input} />
                  </FormGroup>
                  <FormGroup className={styles.inputContainer}>
                    <i className="fa fa-envelope icon"></i>
                    <Input placeholder="Email" className={styles.input} />
                  </FormGroup>
                  <FormGroup className={styles.inputContainer}>
                    <i className={styles.phone_icon_sign}></i>
                    <Dropdown title="+91" className={styles.Dropdown_sign}>
                      <Dropdown.Item>+92</Dropdown.Item>
                      <Dropdown.Item>+93</Dropdown.Item>
                      <Dropdown.Item>+94</Dropdown.Item>
                      <Dropdown.Item>+95</Dropdown.Item>
                    </Dropdown>
                    <Input
                      placeholder="Phone Number"
                      className={styles.input_phone_sign}
                    />
                  </FormGroup>
                </Form>

                <div className={styles.Sign_box}>
                  <Checkbox>
                    {" "}
                    <p className={styles.checkbox}>
                      {" "}
                      I have read and agree to the{" "}
                      <a href="#">Terms of Service</a>
                    </p>
                  </Checkbox>
                </div>
                <div className={styles.btn}>Create Account</div>
              </div>
              <div className="col-4 col-sm-4">
                <div className={styles.ImageBox}>
                  <p className={styles.para1}></p>
                  <p className={styles.para2}></p>
                  <p className={styles.para3}></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default SignUp;
